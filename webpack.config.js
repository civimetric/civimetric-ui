const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const sass = require("node-sass")
const path = require("path")
const config = require("sapper/config/webpack.js")
const webpack = require("webpack")
const WebpackModules = require("webpack-modules")

const pkg = require("./package.json")

const mode = process.env.NODE_ENV || "production"
const dev = mode === "development"

const alias = { svelte: path.resolve("node_modules", "svelte") }
const extensions = [".mjs", ".js", ".json", ".svelte", ".html"]
const mainFields = ["svelte", "module", "browser", "main"]

module.exports = {
  client: {
    entry: config.client.entry(),
    output: config.client.output(),
    resolve: { alias, extensions, mainFields },
    module: {
      rules: [
        {
          test: /(\.m?js|\.html|\.svelte)$/,
          exclude: /node_modules\/(?!svelte)/, // (?!svelte) - to transpile svelte components from node_modules
          loader: "babel-loader",
        },
        {
          test: /\.(html|svelte)$/,
          use: {
            loader: "svelte-loader",
            options: {
              dev,
              hydratable: true,
              hotReload: false, // pending https://github.com/sveltejs/svelte/issues/2377
              preprocess: {
                style: ({ content, attributes }) => {
                  if (attributes.type !== "text/scss") return

                  return new Promise((fulfil, reject) => {
                    sass.render(
                      {
                        data: content,
                        includePaths: ["components", "routes"],
                        sourceMap: true,
                        importer: function (url) {
                          if (url.indexOf("~") === 0) {
                            const nodeModulePath = `./node_modules/${url.substr(
                              1,
                            )}`
                            return {
                              file: require("path").resolve(nodeModulePath),
                            }
                          }
                          return { file: url }
                        },
                        outFile: "x", // this is necessary, but is ignored
                      },
                      (err, result) => {
                        if (err) {
                          return reject(err)
                        }
                        fulfil({
                          code: result.css,
                          map: result.map,
                        })
                      },
                    )
                  })
                },
              },
            },
          },
        },
        {
          test: /(\.m?js|\.html|\.svelte)$/,
          exclude: /node_modules\/(?!svelte)/, // (?!svelte) - to transpile svelte components from node_modules
          loader: "eslint-loader",
          options: {
            emitWarning: dev,
            fix: true,
          },
        },
      ],
    },
    mode,
    plugins: [
      // pending https://github.com/sveltejs/svelte/issues/2377
      // dev && new webpack.HotModuleReplacementPlugin(),
      new webpack.DefinePlugin({
        "process.browser": true,
        "process.env.NODE_ENV": JSON.stringify(mode),
      }),
    ].filter(Boolean),
    devtool: dev && "inline-source-map",
  },
  server: {
    entry: config.server.entry(),
    output: config.server.output(),
    target: "node",
    resolve: { alias, extensions, mainFields },
    externals: Object.keys(pkg.dependencies).concat("encoding"),
    module: {
      rules: [
        {
          test: /\.(html|svelte)$/,
          use: {
            loader: "svelte-loader",
            options: {
              css: false,
              dev,
              generate: "ssr",
              hydratable: true,
              preprocess: {
                style: ({ content, attributes }) => {
                  if (attributes.type !== "text/scss") return

                  return new Promise((fulfil, reject) => {
                    sass.render(
                      {
                        data: content,
                        includePaths: ["components", "routes"],
                        sourceMap: true,
                        importer: function (url) {
                          if (url.indexOf("~") === 0) {
                            const nodeModulePath = `./node_modules/${url.substr(
                              1,
                            )}`
                            return {
                              file: require("path").resolve(nodeModulePath),
                            }
                          }
                          return { file: url }
                        },
                        outFile: "x", // this is necessary, but is ignored
                      },
                      (err, result) => {
                        if (err) {
                          return reject(err)
                        }
                        fulfil({
                          code: result.css,
                          map: result.map,
                        })
                      },
                    )
                  })
                },
              },
            },
          },
        },
        {
          test: /\.(scss)$/,
          use: [
            MiniCssExtractPlugin.loader,
            "css-loader", // translates CSS into CommonJS modules
            {
              loader: "postcss-loader", // Run post css actions
              options: {
                postcssOptions: {
                  plugins: [
                    // post css plugins, can be exported to postcss.config.js
                    [
                      require("postcss-url")(),
                      require("precss"),
                      require("autoprefixer"),
                    ],
                  ],
                },
              },
            },
            "sass-loader", // compiles Sass to CSS
          ],
        },
        {
          test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
          use: {
            loader: "url-loader",
          },
        },
      ],
    },
    plugins: [
      new WebpackModules(),
      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: "../../../static/[name].css",
        chunkFilename: "[id].css",
      }),
    ],
    mode,
    performance: {
      hints: false, // it doesn't matter if server.js is large
    },
  },
  serviceworker: {
    entry: config.serviceworker.entry(),
    output: config.serviceworker.output(),
    mode,
  },
}
