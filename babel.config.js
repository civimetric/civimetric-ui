module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        bugfixes: true,
        corejs: 3,
        targets: "> 0.25%, not dead",
        useBuiltIns: "usage",
      },
    ],
  ],
  plugins: ["@babel/plugin-syntax-dynamic-import"],
}
