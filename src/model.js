// Server-only functions

import fs from "fs"
import path from "path"

import serverConfig from "./server-config"
import { slugify } from "./helpers"

const { accountsDir, answersDir, organizationsDir, usersDir } = serverConfig

export function checkToken(organization, survey, token) {
  if (!survey.tokens) {
    return true
  }
  const tokensFilePath = path.join(accountsDir, organization.slug, survey.slug, "tokens.json")
  const tokens = JSON.parse(fs.readFileSync(tokensFilePath, { encoding: "utf-8" }))
  return tokens.includes(token)
}

export function getOrganization(organizationSlug) {
  const organizationDir = path.join(organizationsDir, organizationSlug)
  if (!fs.existsSync(organizationDir)) {
    return null
  }
  if (!fs.lstatSync(organizationDir).isDirectory()) {
    return null
  }
  const organizationFilePath = path.join(organizationDir, "organization.json")
  if (!fs.existsSync(organizationFilePath)) {
    return null
  }
  if (!fs.lstatSync(organizationFilePath).isFile()) {
    return null
  }
  const organizationText = fs.readFileSync(organizationFilePath)
  return JSON.parse(organizationText)
}

export function getOrganizations() {
  return fs
    .readdirSync(organizationsDir)
    .filter((filename) => !filename.startsWith("."))
    .map((filename) => path.join(organizationsDir, filename))
    .filter((filePath) => fs.lstatSync(filePath).isDirectory())
    .map((organizationDir) => path.join(organizationDir, "organization.json"))
    .filter(
      (organizationFilePath) => fs.existsSync(organizationFilePath) && fs.lstatSync(organizationFilePath).isFile(),
    )
    .map((organizationFilePath) => {
      const organizationText = fs.readFileSync(organizationFilePath)
      return JSON.parse(organizationText)
    })
    .filter((organization) => organization !== null)
}

export function getSurvey(organization, surveySlug) {
  const surveyDir = path.join(organization.dir, surveySlug)
  if (!fs.existsSync(surveyDir)) {
    return null
  }
  if (!fs.lstatSync(surveyDir).isDirectory()) {
    return null
  }
  const surveyFilePath = path.join(surveyDir, "survey.json")
  if (!fs.existsSync(surveyFilePath)) {
    return null
  }
  if (!fs.lstatSync(surveyFilePath).isFile()) {
    return null
  }
  let surveyText = fs.readFileSync(surveyFilePath)
  return JSON.parse(surveyText)
}

export function getSurveyAnswers(organization, survey) {
  const surveyAnswersDir = path.join(answersDir, organization.slug, survey.slug)
  if (!fs.existsSync(surveyAnswersDir)) {
    return null
  }
  if (!fs.lstatSync(surveyAnswersDir).isDirectory()) {
    return null
  }

  return fs
    .readdirSync(surveyAnswersDir)
    .filter((filename) => !filename.startsWith(".") && filename.endsWith(".json"))
    .map((filename) => path.join(surveyAnswersDir, filename))
    .map((surveyAnswerFilePath) => {
      const surveyAnswerText = fs.readFileSync(surveyAnswerFilePath)
      const surveyAnswer = JSON.parse(surveyAnswerText)
      const token = path.basename(surveyAnswerFilePath, ".json")
      return [token, surveyAnswer]
    })
    .filter(([token, surveyAnswer]) => surveyAnswer !== null) // eslint-disable-line no-unused-vars
    .reduce((surveyAnswerByToken, [token, surveyAnswer]) => {
      surveyAnswerByToken[token] = surveyAnswer
      return surveyAnswerByToken
    }, {})
}

export function getSurveys(organizationDir) {
  return fs
    .readdirSync(organizationDir)
    .filter((filename) => !filename.startsWith("."))
    .map((filename) => path.join(organizationDir, filename))
    .filter((filePath) => fs.lstatSync(filePath).isDirectory())
    .map((surveyDir) => path.join(surveyDir, "survey.json"))
    .filter((surveyFilePath) => fs.existsSync(surveyFilePath) && fs.lstatSync(surveyFilePath).isFile())
    .map((surveyFilePath) => {
      const surveyText = fs.readFileSync(surveyFilePath)
      return JSON.parse(surveyText)
    })
    .filter((survey) => survey !== null)
}

export function getUser(email) {
  const slug = slugify(email)
  if (!slug) {
    return null
  }
  if (!fs.existsSync(usersDir)) {
    return null
  }
  if (!fs.lstatSync(usersDir).isDirectory()) {
    return null
  }
  const userFilePath = path.join(usersDir, `${slug}.json`)
  if (!fs.existsSync(userFilePath)) {
    return null
  }
  if (!fs.lstatSync(userFilePath).isFile()) {
    return null
  }
  const userText = fs.readFileSync(userFilePath)
  return JSON.parse(userText)
}

export function toUserJson(user, { showApiKey = false, showEmail = false } = {}) {
  let userJson = { ...user }
  if (!showApiKey) delete userJson.apiKey
  if (!showEmail) delete userJson.email
  // userJson.createdAt = userJson.createdAt.toISOString()
  // delete userJson.id
  delete userJson.passwordDigest
  delete userJson.salt
  return userJson
}
