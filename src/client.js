import "bootstrap"

import * as sapper from "@sapper/app"

sapper.start({
  // eslint-disable-next-line no-undef
  target: document.querySelector("#sapper"),
})
