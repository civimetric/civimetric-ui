import commandLineArgs from "command-line-args"

import { generateOrganization } from "./generators"

const optionsDefinition = [{ alias: "o", defaultOption: true, name: "organization", type: String }]
const options = commandLineArgs(optionsDefinition)
const organizationSlug = options["organization"]
// eslint-disable-next-line no-unused-vars
const [organization, error] = generateOrganization(organizationSlug)
if (error !== null) {
  console.error(JSON.stringify(error, null, 2))
  process.exit(1)
}
process.exit(0)
