import originalSlugify from "slug"

export function camelCaseToDashed(s) {
  return s.replace(/([A-Z])/g, (segment) => "-" + segment.toLowerCase())
}

export function collectSelectQuestionAnswers(survey, block, question, surveyAnswers) {
  const answers = []
  const tokens = []
  const multipleAnswers = question.bounds[1] > 1
  for (let [token, surveyAnswer] of Object.entries(surveyAnswers)) {
    const blockAnswers = surveyAnswer[block.slug]
    if (blockAnswers === undefined) {
      continue
    }
    const questionAnswers = blockAnswers[question.slug]
    if (questionAnswers === undefined) {
      continue
    }
    if (multipleAnswers) {
      for (let questionAnswer of questionAnswers) {
        answers.push(questionAnswer)
        tokens.push(token)
      }
    } else {
      answers.push(questionAnswers)
      tokens.push(token)
    }
  }
  return { answers, tokens }
}

export function countAnswers(answers, range) {
  const counts = Array(range).fill(0)
  for (let answer of answers) {
    counts[answer] += 1
  }
  return counts
}

export function dashedToCamelCase(s) {
  return s.replace(/(-[a-z])/g, (segment) => segment.toUpperCase().replace("-", ""))
}

// Cf https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
export function shuffle(array) {
  array = [...array]
  let currentIndex = array.length

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    const randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

    // And swap it with the current element.
    const temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }

  return array
}

const slugifyCharmap = {
  ...originalSlugify.defaults.charmap,
  "'": " ",
  "@": " ",
  ".": " ",
}

export function slugify(string, replacement) {
  const options = {
    charmap: slugifyCharmap,
    mode: "rfc3986",
  }
  if (replacement) {
    options.replacement = replacement
  }
  return originalSlugify(string, options)
}
