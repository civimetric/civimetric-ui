import commandLineArgs from "command-line-args"

import { generateSurvey } from "./generators"
import { getOrganization } from "./model"

const optionsDefinition = [
  { alias: "o", name: "organization", type: String },
  { alias: "s", name: "survey", type: String },
]
const options = commandLineArgs(optionsDefinition)
const organizationSlug = options["organization"]
const surveySlug = options["survey"]
const organization = getOrganization(organizationSlug)
// eslint-disable-next-line no-unused-vars
const [survey, error] = generateSurvey(organization, surveySlug)
if (error !== null) {
  console.error(JSON.stringify(error, null, 2))
  process.exit(1)
}
process.exit(0)
