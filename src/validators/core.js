// Client & server core validators

// Cf https://stackoverflow.com/questions/4460586/javascript-regular-expression-to-check-for-ip-addresses
const ipV4AddressRegExp = new RegExp(
  "" +
    /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/.source +
    /\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/.source +
    /\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/.source +
    /\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.source,
)

// Cf https://home.deds.nl/~aeron/regex/
const ipV6AddressRegExp = new RegExp(
  "" +
    /^((?=.*::)(?!.*::.+::)(::)?([\dA-F]{1,4}:(:|\b)|){5}|([\dA-F]{1,4}:){6})/.source +
    /((([\dA-F]{1,4}((?!\3)::|:\b|$))|(?!\2\3)){2}|(((2[0-4]|1\d|[1-9])?\d|25[0-5])\.?\b){4})$/i.source,
)

export const shortTokenRegExp = /^[0-9a-z]{6}$/
export const uuidTokenRegExp = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/

export function validateArray(itemValidator) {
  return function (array) {
    if (array === null || array === undefined) {
      return [array, "Missing value"]
    }
    if (!Array.isArray(array)) {
      return [array, `Expected an array, got "${typeof array}"`]
    }
    const errors = {}
    array = [...array]
    for (let [index, value] of array.entries()) {
      const [validatedValue, error] = itemValidator(value)
      array[index] = validatedValue
      if (error !== null) {
        errors[index] = error
      }
    }
    return [array, Object.keys(errors).length === 0 ? null : errors]
  }
}

export function validateBoolean(value) {
  if (value === null || value === undefined) {
    return [value, "Missing value"]
  }
  if (![false, true].includes(value)) {
    return [value, `Expected a boolean got "${typeof value}"`]
  }
  return [value, null]
}

export function validateChoice(options) {
  return function (input) {
    let [value, error] = validateNonEmptyTrimmedString(input)
    if (error !== null) {
      return [value, error]
    }
    if (!options.includes(value)) {
      return [value, "Unexpected option"]
    }
    return [value, null]
  }
}

export function validateDateTimeString(input) {
  let [value, error] = validateNonEmptyTrimmedString(input)
  if (error !== null) {
    return [value, error]
  }
  // Cf https://stackoverflow.com/questions/3143070/javascript-regex-iso-datetime
  if (!/^\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)$/.test(value)) {
    return [value, "Expected a date and time in ISO format"]
  }
  return [value, null]
}

export function validateInteger(value) {
  if (value === null || value === undefined) {
    return [value, "Missing value"]
  }
  if (isNaN(value) || !Number.isInteger(value)) {
    return [value, `Expected an integer got "${typeof value}"`]
  }
  return [value, null]
}

export function validateIpAddress(input) {
  let [value, error] = validateNonEmptyTrimmedString(input)
  if (error !== null) {
    return [value, error]
  }
  if (!ipV4AddressRegExp.test(value) && !ipV6AddressRegExp.test(value)) {
    return [value, "Expected an IP address"]
  }
  return [value, null]
}

export function validateJson(dataValidator) {
  return function (jsonText) {
    if (jsonText === null || jsonText === undefined) {
      return [jsonText, "Missing value"]
    }
    let data = null
    try {
      data = JSON.parse(jsonText)
    } catch (error) {
      return [jsonText, error]
    }
    return dataValidator(data)
  }
}

export function validateMaybeBoolean(value) {
  if (value === null || value === undefined) {
    return [false, null]
  }
  if (![false, true].includes(value)) {
    return [value, `Expected a boolean got "${typeof value}"`]
  }
  return [value, null]
}

export function validateMayBeTrimmedString(value) {
  if (value === null || value === undefined) {
    return [null, null]
  }
  if (typeof value !== "string") {
    return [value, `Expected a string got "${typeof value}"`]
  }
  value = value.trim()
  if (!value) {
    return [null, null]
  }
  return [value, null]
}

export function validateNonEmptyTrimmedString(value) {
  if (value === null || value === undefined) {
    return [value, "Missing value"]
  }
  if (typeof value !== "string") {
    return [value, `Expected a string got "${typeof value}"`]
  }
  value = value.trim()
  if (!value) {
    return [value, "Expected a non empty string"]
  }
  return [value, null]
}

export function validateRegExp(regExp, errorMessage) {
  return function (input) {
    let [value, error] = validateNonEmptyTrimmedString(input)
    if (error !== null) {
      return [value, error]
    }
    if (!regExp.test(value)) {
      return [value, errorMessage]
    }
    return [value, null]
  }
}
