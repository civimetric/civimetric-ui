// Client & server validators for survey answers

import { validateDateTimeString, validateIpAddress } from "./core"

export function validateBlockAnswer(survey, block, answer) {
  return block.type === "demographic"
    ? validateDemographicBlockAnswer(survey, block, answer)
    : block.type === "dynamic grid"
    ? validateDynamicGridBlockAnswer(survey, block, answer)
    : block.type === "importance"
    ? validateImportanceBlockAnswer(survey, block, answer)
    : block.type === "information"
    ? validateInformationBlockAnswer(survey, block, answer)
    : block.type === "static grid"
    ? validateStaticGridBlockAnswer(survey, block, answer)
    : [answer, `Missing validation code for block of type "${block.type}"`]
}

export function validateDemographicBlockAnswer(survey, block, answer) {
  if (answer === null || answer === undefined) {
    answer = {}
  } else if (typeof answer === "object") {
    answer = { ...answer }
  } else {
    return [answer, `Answer for block of type "${block.type}" must be an object`]
  }
  const remainingAnswerKeys = new Set(Object.keys(answer))
  const errors = {}
  for (let question of block.questions) {
    remainingAnswerKeys.delete(question.slug)
    const [questionAnswer, questionError] = validateQuestionAnswer(question, answer[question.slug])
    answer[question.slug] = questionAnswer
    if (questionError !== null) {
      errors[question.slug] = questionError
    }
  }
  for (let key of remainingAnswerKeys) {
    errors[key] = `Block has no question named "${key}"`
  }
  return [answer, Object.keys(errors).length === 0 ? null : errors]
}

export function validateDynamicGridBlockAnswer(survey, block, answer) {
  if (answer === null || answer === undefined) {
    answer = {}
  } else if (typeof answer === "object") {
    answer = { ...answer }
  } else {
    return [answer, `Answer for block of type "${block.type}" must be an object`]
  }
  const grid = survey.grids[block.grid]
  if (grid === undefined) {
    return [answer, `Missing grid "${block.grid}"`]
  }
  const remainingAnswerKeys = new Set(Object.keys(answer))
  const errors = {}
  let totalSpending = 0
  for (let question of grid) {
    remainingAnswerKeys.delete(question.slug)
    let questionAnswer = answer[question.slug]
    if (questionAnswer === null || questionAnswer === undefined) {
      errors[question.slug] = "Missing required value"
      continue
    }
    if (!Number.isInteger(questionAnswer)) {
      errors[question.slug] = "Answer must be an integer"
      continue
    }
    if (questionAnswer < 0 || questionAnswer >= question.options.length) {
      errors[question.slug] = `Answer must be an integer between 0 and ${question.options.length - 1}`
      continue
    }
    totalSpending += question.prices[questionAnswer]
  }
  for (let key of remainingAnswerKeys) {
    errors[key] = `Block has no question named "${key}"`
  }
  if (Object.keys(errors).length !== 0) {
    return [answer, errors]
  }
  if (totalSpending > block.spending_limit + block.tolerance) {
    return [
      answer,
      `Total spending (${totalSpending}) must be lesser than ${block.spending_limit + block.tolerance + 1}`,
    ]
  }
  return [answer, null]
}

export function validateImportanceBlockAnswer(survey, block, answer) {
  if (answer === null || answer === undefined) {
    answer = {}
  } else if (typeof answer === "object") {
    answer = { ...answer }
  } else {
    return [answer, `Answer for block of type "${block.type}" must be an object`]
  }
  const remainingAnswerKeys = new Set(Object.keys(answer))
  const errors = {}

  const question = { ...block, required: true, slug: "importance" }
  remainingAnswerKeys.delete(question.slug)
  const [questionAnswer, questionError] = validateImportanceQuestionAnswer(
    survey,
    block,
    question,
    answer[question.slug],
  )
  answer[question.slug] = questionAnswer
  if (questionError !== null) {
    errors[question.slug] = questionError
  }

  for (let key of remainingAnswerKeys) {
    errors[key] = `Block has no question named "${key}"`
  }
  return [answer, Object.keys(errors).length === 0 ? null : errors]
}

export function validateImportanceQuestionAnswer(survey, block, question, answer) {
  if (answer === null || answer === undefined) {
    return [null, question.required ? "Missing required value" : null]
  }
  const bounds = question.bounds
  const grid = survey.grids[question.grid]
  if (grid === undefined) {
    return [answer, `Missing grid "${question.grid}"`]
  }
  const options = grid.map((question) => question.slug)
  if (bounds[1] === 1) {
    if (typeof answer !== "string") {
      return [answer, `Answer to question of type "${question.type}" must be a string`]
    }
    if (!options.includes(answer)) {
      return [answer, `Answer must belong to set {${options.join(", ")}}`]
    }
  } else {
    if (!Array.isArray(answer)) {
      return [answer, `Answer to question of type "${question.type}" must be an array`]
    }
    if (answer.length < bounds[0] || answer.length > bounds[1]) {
      return [answer, `Answer to question must be an array containing between ${bounds[0]} and ${bounds[1]} items`]
    }
    if (new Set(answer).size !== answer.length) {
      return [answer, "Answer to question must be an array containing distinct items"]
    }
    answer = [...answer]
    const errors = {}
    for (let [itemIndex, itemAnswer] of answer.entries()) {
      if (itemAnswer === null || itemAnswer === undefined || typeof itemAnswer !== "string") {
        errors[itemIndex] = "Answer to question must be an array of strings"
        continue
      }
      if (!options.includes(itemAnswer)) {
        errors[itemIndex] = `Answer must belong to set {${options.join(", ")}}`
        continue
      }
    }
    if (Object.keys(errors).length > 0) {
      return [answer, errors]
    }
    answer.sort()
  }
  return [answer, null]
}

export function validateInformationBlockAnswer(survey, block, answer) {
  if (answer !== null && answer !== undefined) {
    return [answer, `Block of type "${block.type}" doesn't expect any answer`]
  }
  return [null, null]
}

export function validateQuestionAnswer(question, questionAnswer) {
  return question.type === "select"
    ? validateSelectQuestionAnswer(question, questionAnswer)
    : question.type === "textarea"
    ? validateTextAreaQuestionAnswer(question, questionAnswer)
    : [questionAnswer, `Missing validation code for question of type "${question.type}"`]
}

export function validateSelectQuestionAnswer(question, answer) {
  if (answer === null || answer === undefined) {
    return [null, question.required ? "Missing required value" : null]
  }
  const bounds = question.bounds
  if (bounds[1] === 1) {
    if (!Number.isInteger(answer)) {
      return [answer, `Answer to question of type "${question.type}" must be an integer`]
    }
    if (answer < 0 || answer >= question.options.length) {
      return [answer, `Answer must be an integer between 0 and ${question.options.length - 1}`]
    }
  } else {
    if (!Array.isArray(answer)) {
      return [answer, `Answer to question of type "${question.type}" must be an array`]
    }
    if (answer.length < bounds[0] || answer.length > bounds[1]) {
      return [answer, `Answer to question must be an array containing between ${bounds[0]} and ${bounds[1]} items`]
    }
    if (new Set(answer).size !== answer.length) {
      return [answer, "Answer to question must be an array containing distinct items"]
    }
    answer = [...answer]
    const errors = {}
    for (let [itemIndex, itemAnswer] of answer.entries()) {
      if (!Number.isInteger(itemAnswer)) {
        errors[itemIndex] = "Answer to question must be an array of integers"
        continue
      }
      if (itemAnswer < 0 || itemAnswer >= question.options.length) {
        errors[itemIndex] = `Answer must be an integer between 0 and ${question.options.length - 1}`
        continue
      }
    }
    if (Object.keys(errors).length > 0) {
      return [answer, errors]
    }
    answer.sort((a, b) => a - b)
  }
  return [answer, null]
}

export function validateStaticGridBlockAnswer(survey, block, answer) {
  if (answer === null || answer === undefined) {
    answer = {}
  } else if (typeof answer === "object") {
    answer = { ...answer }
  } else {
    return [answer, `Answer for block of type "${block.type}" must be an object`]
  }
  const grid = survey.grids[block.grid]
  if (grid === undefined) {
    return [answer, `Missing grid "${block.grid}"`]
  }

  const remainingAnswerKeys = new Set(Object.keys(answer))
  const errors = {}
  for (let question of grid) {
    const acceptableKey = `acceptable.${question.slug}`
    remainingAnswerKeys.delete(acceptableKey)
    const acceptableQuestion = {
      bounds: [1, question.options.length],
      options: question.options,
      required: false,
      slug: question.slug,
      type: block.type,
    }
    const [acceptableQuestionAnswer, acceptableQuestionError] = validateSelectQuestionAnswer(
      acceptableQuestion,
      answer[acceptableKey],
    )
    answer[acceptableKey] = acceptableQuestionAnswer
    if (acceptableQuestionError !== null) {
      errors[acceptableKey] = acceptableQuestionError
    }

    const currentKey = `current.${question.slug}`
    remainingAnswerKeys.delete(currentKey)
    const currentQuestion = {
      bounds: [1, 1],
      options: question.options,
      required: false,
      slug: question.slug,
      type: block.type,
    }
    const [currentQuestionAnswer, currentQuestionError] = validateSelectQuestionAnswer(
      currentQuestion,
      answer[currentKey],
    )
    answer[currentKey] = currentQuestionAnswer
    if (currentQuestionError !== null) {
      errors[currentKey] = currentQuestionError
    }
  }
  for (let key of remainingAnswerKeys) {
    errors[key] = `Block has no question named "${key}"`
  }

  return [answer, Object.keys(errors).length === 0 ? null : errors]
}

export function validateSurveyAnswer(survey, answer, ipAddress) {
  if (answer === null || typeof answer !== "object") {
    return [answer, "Answer for survey must be an object"]
  }
  answer = {
    ...answer,
    ipAddress,
    stopDateTime: new Date().toISOString(),
  }
  const errors = {}
  const remainingAnswerKeys = new Set(Object.keys(answer))

  {
    const key = "ipAddress"
    if (remainingAnswerKeys.delete(key)) {
      const [value, error] = validateIpAddress(answer[key])
      answer[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  for (let key of ["startDateTime", "stopDateTime"]) {
    if (remainingAnswerKeys.delete(key)) {
      const [value, error] = validateDateTimeString(answer[key])
      answer[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  for (let block of survey.blocks) {
    remainingAnswerKeys.delete(block.slug)
    const [blockAnswer, blockError] = validateBlockAnswer(survey, block, answer[block.slug])
    answer[block.slug] = blockAnswer
    if (blockError !== null) {
      errors[block.slug] = blockError
    }
  }

  for (let key of remainingAnswerKeys) {
    errors[key] = `Survey has no block named "${key}"`
  }
  return [answer, Object.keys(errors).length === 0 ? null : errors]
}

export function validateTextAreaQuestionAnswer(question, answer) {
  if (answer === null || answer === undefined) {
    return [null, question.required ? "Missing required value" : null]
  }
  if (typeof answer !== "string") {
    return [answer, `Answer to question of type "${question.type}" must be a string`]
  }
  answer = answer.trim()
  if (question.required && !answer) {
    return [answer, "Missing required value"]
  }
  return [answer, null]
}
