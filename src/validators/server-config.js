import fs from "fs"
import path from "path"

import { validateNonEmptyTrimmedString } from "./core"

export function validateServerConfig(config) {
  if (config === null || config === undefined) {
    return [config, "Missing config"]
  }
  if (typeof config !== "object") {
    return [config, `Expected an object got "${typeof config}"`]
  }

  config = { ...config }
  const errors = {}
  const remainingKeys = new Set(Object.keys(config))

  {
    const key = "dataDir"
    if (remainingKeys.delete(key)) {
      let dir = config[key]
      if (typeof dir !== "string") {
        errors[key] = `Expected a string got "${typeof config}"`
      } else {
        dir = path.resolve(path.normalize(path.join("src/", dir.trim())))
        if (!fs.existsSync(dir)) {
          try {
            fs.mkdirSync(dir)
          } catch (e) {
            errors[key] = `Creation of directory "${dir}" failed: ${e}`
          }
        } else {
          if (!fs.lstatSync(dir).isDirectory()) {
            errors[key] = `Node "${dir}" isn't a directory`
          }
        }
      }
      config[key] = dir
    } else {
      errors[key] = "Missing item"
    }
  }

  for (let [key, dirName] of [
    ["accountsDir", "accounts"],
    ["answersDir", "answers"],
    ["badAnswersDir", "bad_answers"],
    ["organizationsDir", "organizations"],
    ["sessionsDir", "sessions"],
    ["usersDir", path.join("accounts", "users")],
  ]) {
    remainingKeys.delete(key)
    let dir = config[key]
    if ((dir === null || dir === undefined) && !errors.dataDir) {
      dir = path.join(config.dataDir, dirName)
    } else if (typeof dir === "string") {
      dir = path.resolve(path.normalize(path.join("src/", dir.trim())))
    }
    if (typeof dir !== "string") {
      errors[key] = `Expected a string got "${typeof config}"`
    } else {
      if (!fs.existsSync(dir)) {
        try {
          fs.mkdirSync(dir)
        } catch (e) {
          errors[key] = `Creation of directory "${dir}" failed: ${e}`
        }
      } else {
        if (!fs.lstatSync(dir).isDirectory()) {
          errors[key] = `Node "${dir}" isn't a directory`
        }
      }
    }
    config[key] = dir
  }

  {
    const key = "sessionSecret"
    if (remainingKeys.delete(key)) {
      const [value, error] = validateNonEmptyTrimmedString(config[key])
      config[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  {
    const key = "title"
    if (remainingKeys.delete(key)) {
      const [value, error] = validateNonEmptyTrimmedString(config[key])
      config[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [config, Object.keys(errors).length === 0 ? null : errors]
}
