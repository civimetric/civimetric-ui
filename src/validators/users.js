import fs from "fs"
import path from "path"

import { slugify } from "../helpers"
import serverConfig from "../server-config"
import { validateNonEmptyTrimmedString } from "./core"

const { usersDir } = serverConfig

export function validateNewUser(user) {
  if (user === null || user === undefined) {
    return [user, "Missing user"]
  }
  if (typeof user !== "object") {
    return [user, `Expected an object got "${typeof user}"`]
  }

  user = { ...user }
  const errors = {}
  const remainingKeys = new Set(Object.keys(user))

  {
    const key = "email"
    if (remainingKeys.delete(key)) {
      const [value, error] = validateNonEmptyTrimmedString(user[key])
      user[key] = value
      if (error === null) {
        const slug = slugify(value)
        const userFilePath = path.join(usersDir, `${slug}.json`)
        if (fs.existsSync(userFilePath)) {
          error = "An user with the same email address already exists"
        }
      }
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  {
    const key = "password"
    if (remainingKeys.delete(key)) {
      const [value, error] = validateNonEmptyTrimmedString(user[key])
      user[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [user, Object.keys(errors).length === 0 ? null : errors]
}
