// Server-side validators for files

import fs from "fs"

export function validateMayBeTextFile(textValidator) {
  return function (filePath) {
    if (!fs.existsSync(filePath)) {
      return [null, null]
    }
    if (!fs.lstatSync(filePath).isFile()) {
      return [filePath, "Not a file"]
    }
    let text = null
    try {
      text = fs.readFileSync(filePath, "utf-8")
    } catch (error) {
      return [filePath, `Not a valid UTF-8-encoded text file: ${error}`]
    }
    return textValidator(text)
  }
}
