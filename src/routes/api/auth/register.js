import { pbkdf2Sync, randomBytes } from "crypto"
import fs from "fs"
import path from "path"

import { slugify } from "../../../helpers"
import { toUserJson } from "../../../model"
import serverConfig from "../../../server-config"
import { validateNewUser } from "../../../validators/users"

const { usersDir } = serverConfig

export async function post(req, res) {
  const [newUser, error] = validateNewUser(req.body)
  if (error !== null) {
    res.setHeader("Content-Type", "application/json")
    return res.end(JSON.stringify({ ...newUser, error }, null, 2))
  }

  const { email, password } = newUser
  // See http://security.stackexchange.com/a/27971 for explaination of digest and salt size.
  const salt = randomBytes(16).toString("base64").replace(/=/g, "")
  const slug = slugify(email)
  const user = {
    activated: false,
    apiKey: randomBytes(16).toString("base64").replace(/=/g, ""), // 128 bits API key
    email,
    isAdmin: false,
    passwordDigest: pbkdf2Sync(password, salt, 4096, 16, "sha512").toString("base64").replace(/=/g, ""),
    salt,
    slug,
  }

  const userFilePath = path.join(usersDir, `${slug}.json`)
  fs.writeFileSync(userFilePath, JSON.stringify(user, null, 2))

  res.setHeader("Content-Type", "application/json")
  return res.end(JSON.stringify(toUserJson(user, { showApiKey: true, showEmail: true }), null, 2))
}
