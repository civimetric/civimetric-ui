import passport from "passport"

import { toUserJson } from "../../../model"

export function post(req, res, next) {
  passport.authenticate("local", function (err, user, info) {
    if (err) {
      return next(err)
    }
    if (!user) {
      res.setHeader("Content-Type", "application/json")
      return res.end(JSON.stringify({ error: info }, null, 2))
    }
    req.login(user, function (err) {
      if (err) {
        return next(err)
      }
      res.setHeader("Content-Type", "application/json")
      return res.end(JSON.stringify(toUserJson(user, { showApiKey: true, showEmail: true }), null, 2))
    })
  })(req, res, next)
}
