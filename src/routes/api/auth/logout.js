import { toUserJson } from "../../../model"

export function post(req, res) {
  const user = req.user
  req.logout()
  // // Note: Web browsers and other compliant clients will only clear the cookie if the given
  // // options is identical to those given to res.cookie(), excluding expires and maxAge.
  // res.clearCookie("connect.sid", {
  //   httpOnly: true,
  //   path: "/",
  // })
  // req.session.destroy(function () {
  //   res.setHeader("Content-Type", "application/json")
  //   res.end(JSON.stringify(toUserJson(user, { showApiKey: false, showEmail: false }), null, 2))
  // })
  res.setHeader("Content-Type", "application/json")
  res.end(JSON.stringify(toUserJson(user, { showApiKey: false, showEmail: false }), null, 2))
}
