import { getOrganizations } from "../../model"

export function get(req, res) {
  const organizations = getOrganizations()
  res.writeHead(200, {
    "Content-Type": "application/json",
  })
  res.end(
    JSON.stringify(
      {
        organizations,
      },
      null,
      2,
    ),
  )
}
