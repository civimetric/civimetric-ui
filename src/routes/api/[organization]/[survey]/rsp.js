import { getOrganization, getSurvey, getSurveyAnswers } from "../../../../model"

function generateSurveyRsp(survey, surveyAnswers) {
  const countByIpAddressDigest = {}
  for (let surveyAnswer of Object.values(surveyAnswers)) {
    const ipAddressDigest = surveyAnswer.ipAddressDigest
    if (ipAddressDigest) {
      countByIpAddressDigest[ipAddressDigest] = (countByIpAddressDigest[ipAddressDigest] || 0) + 1
    }
  }

  const tokens = Object.keys(surveyAnswers)
  tokens.sort()

  let content = ""
  for (let [answerIndex, token] of tokens.entries(tokens)) {
    const surveyAnswer = surveyAnswers[token]
    if (surveyAnswer.startDateTime) {
      content += `// Started at ${surveyAnswer.startDateTime}\n`
    }
    if (surveyAnswer.stopDateTime) {
      content += `// Submitted at ${surveyAnswer.stopDateTime}\n`
    }
    if (surveyAnswer.ipAddressDigest) {
      const ipAddressDigest = surveyAnswer.ipAddressDigest
      const count = countByIpAddressDigest[ipAddressDigest]
      const countText = count > 1 ? ` (${count} occurrences)` : ""
      content += `// IP address digest = ${ipAddressDigest}${countText}\n`
    }
    if (surveyAnswer.geo) {
      content += `// Region = ${surveyAnswer.geo.region}; Country = ${surveyAnswer.geo.country}; City = ${surveyAnswer.geo.city}\n`
    }
    content += `// Token = ${token}\n`
    content += `${answerIndex + 1}\r\n`
    for (const block of survey.blocks) {
      const blockAnswer = surveyAnswer[block.slug]
      if (!blockAnswer) {
        continue
      }
      if (block.type === "demographic") {
        const indexes = block.questions
          .map((question) => {
            if (question.type === "select") {
              const questionAnswer = blockAnswer[question.slug]
              if (questionAnswer === undefined) {
                return "0"
              } else if (Array.isArray(questionAnswer)) {
                return questionAnswer.map((answer) => (answer + 1).toString()).join(" ")
              } else {
                return (questionAnswer + 1).toString()
              }
            } else if (question.type === "textarea") {
              return null
            } else {
              return `UNKNOWN QUESTION TYPE: ${question.type}\r\n`
            }
          })
          .filter((fragment) => fragment !== null)
          .join(", ")
        if (indexes) {
          content += ` ${indexes}\r\n`
        }
      } else if (block.type === "dynamic grid") {
        const grid = survey.grids[block.grid]
        if (!grid) {
          continue
        }
        const indexes = grid.map((question) => (blockAnswer[question.slug] + 1).toString()).join(", ")
        content += ` ${indexes}\r\n`
      } else if (block.type === "importance") {
        const grid = survey.grids[block.grid]
        if (!grid) {
          continue
        }
        let importanceAnswer = blockAnswer.importance
        if (!importanceAnswer) {
          importanceAnswer = []
        }
        const indexes = grid.map((question) => (importanceAnswer.includes(question.slug) ? "1" : "0")).join(", ")
        content += ` ${indexes}\r\n`
      } else if (block.type === "information") {
        // Ignore this block: it has no question.
      } else if (block.type === "static grid") {
        const grid = survey.grids[block.grid]
        if (!grid) {
          continue
        }

        const currentIndexes = grid
          .map((question) => {
            let questionAnswer = blockAnswer[`current.${question.slug}`]
            if (questionAnswer === null || questionAnswer === undefined) {
              questionAnswer = -1
            }
            return (questionAnswer + 1).toString()
          })
          .join(", ")
        content += ` ${currentIndexes}\r\n`

        // const acceptableIndexes = grid
        //   .map(question => {
        //     const questionAnswers = blockAnswer[`acceptable.${question.slug}`]
        //     return Array.isArray(questionAnswers)
        //       ? questionAnswers.map(questionAnswer => (questionAnswer + 1).toString()).join(" ")
        //       : "0"
        //   })
        //   .join(", ")
        // content += ` ${acceptableIndexes}\r\n`
        const unacceptableIndexes = grid
          .map((question) => {
            let questionAnswers = blockAnswer[`acceptable.${question.slug}`]
            if (!Array.isArray(questionAnswers)) {
              questionAnswers = []
            }
            return (
              [...question.options.keys()]
                .filter((optionIndex) => !questionAnswers.includes(optionIndex))
                .map((questionNonAnswer) => (questionNonAnswer + 1).toString())
                .join(" ") || "0"
            )
          })
          .join(", ")
        content += ` ${unacceptableIndexes}\r\n`
      } else {
        content += ` UNKNOWN BLOCK TYPE: ${block.type}\r\n`
      }
    }
  }

  return content
}

export function get(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthorized: User is not authenticated.",
          },
        },
        null,
        2,
      ),
    )
  }
  if (!user.isAdmin) {
    res.writeHead(401, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Forbidden: User is not an administrator.",
          },
        },
        null,
        2,
      ),
    )
  }

  const { organization: organizationSlug, survey: surveySlug } = req.params
  const organization = getOrganization(organizationSlug)

  if (!organization) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Organization ${organizationSlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  const survey = getSurvey(organization, surveySlug)

  if (!survey) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Survey ${surveySlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  const surveyAnswers = getSurveyAnswers(organization, survey)

  if (!surveyAnswers) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Answers of survey ${surveySlug} don't exist yet.`,
          },
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "text/plain; charset=utf-8",
    "Content-Disposition": `attachment; filename="${survey.slug}.rsp"`,
  })
  res.end(generateSurveyRsp(survey, surveyAnswers))
}
