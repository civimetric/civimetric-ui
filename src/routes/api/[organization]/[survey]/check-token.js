import { checkToken, getOrganization, getSurvey } from "../../../../model"
import { shortTokenRegExp, validateRegExp } from "../../../../validators/core"

export function post(req, res) {
  const { organization: organizationSlug, survey: surveySlug } = req.params
  const organization = getOrganization(organizationSlug)

  if (!organization) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Organization ${organizationSlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  const survey = getSurvey(organization, surveySlug)

  if (!survey) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Survey ${surveySlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  if (!survey.tokens) {
    res.writeHead(400, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message: `Survey ${surveySlug} doesn't use predefined tokens.`,
          },
        },
        null,
        2,
      ),
    )
  }

  const [body, error] = validateBody(req.body)
  if (error !== null) {
    res.writeHead(400, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          body,
          error: {
            code: 400,
            details: error,
            message: `Invalid body for ${organizationSlug}/${surveySlug}/check-token`,
          },
        },
        null,
        2,
      ),
    )
  }

  if (!checkToken(organization, survey, body.token)) {
    res.writeHead(400, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          body,
          error: {
            code: 400,
            message: `Invalid token for ${organizationSlug}/${surveySlug}/check-token`,
          },
        },
        null,
        2,
      ),
    )
  }

  res.statusCode = 204
  res.end()
}

function validateBody(body) {
  if (body === null || body === undefined) {
    return [body, "Missing body"]
  }
  if (typeof body !== "object") {
    return [body, `Expected an object, got ${typeof body}`]
  }

  body = { ...body }
  const remainingKeys = new Set(Object.keys(body))
  const errors = {}

  {
    const key = "token"
    remainingKeys.delete(key)
    const [value, error] = validateRegExp(shortTokenRegExp, "Expected a short token")(body[key])
    body[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [body, Object.keys(errors).length === 0 ? null : errors]
}
