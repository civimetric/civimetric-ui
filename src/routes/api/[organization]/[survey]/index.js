import { pbkdf2Sync } from "crypto"
import fs from "fs"
import geoip from "geoip-lite"
import path from "path"

import { getOrganization, getSurvey } from "../../../../model"
import serverConfig from "../../../../server-config"
import { shortTokenRegExp, uuidTokenRegExp, validateRegExp } from "../../../../validators/core"
import { validateSurveyAnswer } from "../../../../validators/answers"

const { answersDir, badAnswersDir } = serverConfig

export function get(req, res) {
  const { organization: organizationSlug, survey: surveySlug } = req.params
  const organization = getOrganization(organizationSlug)

  if (!organization) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Organization ${organizationSlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  const survey = getSurvey(organization, surveySlug)

  if (!survey) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Survey ${surveySlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json",
  })
  res.end(JSON.stringify(survey, null, 2))
}

export function post(req, res) {
  const { organization: organizationSlug, survey: surveySlug } = req.params
  const organization = getOrganization(organizationSlug)

  if (!organization) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Organization ${organizationSlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  const survey = getSurvey(organization, surveySlug)

  if (!survey) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Survey ${surveySlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  const ipAddress = req.headers["x-forwarded-for"] || req.connection.remoteAddress
  const [body, error] = validateBody(survey, req.body, ipAddress)
  if (error !== null) {
    console.error(
      `Error in posted survey answer:\n${JSON.stringify(body, null, 2)}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )

    const badAnswerFilePath = path.join(
      badAnswersDir,
      `${new Date().toISOString()}_${organization.slug}_${survey.slug}.json`,
    )
    fs.writeFileSync(badAnswerFilePath, JSON.stringify({ body, error: { answer: error } }, null, 2))

    res.writeHead(400, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          body,
          error: {
            code: 400,
            details: error,
            message: `Invalid body for ${organizationSlug}/${surveySlug}`,
          },
        },
        null,
        2,
      ),
    )
  }

  const { answer, token } = body

  // Geolocate answer using IP address.
  answer.geo = geoip.lookup(answer.ipAddress)

  // Replace IP address with a digest salted with geo, to ensure that it is hard to retrieve IP address.
  answer.ipAddressDigest = pbkdf2Sync(answer.ipAddress, JSON.stringify(answer.geo), 4096, 16, "sha512")
    .toString("base64")
    .replace(/=/g, "")
  delete answer.ipAddress

  const organizationAnswersDir = path.join(answersDir, organization.slug)
  if (!fs.existsSync(organizationAnswersDir)) {
    fs.mkdirSync(organizationAnswersDir)
  }
  const surveyAnswersDir = path.join(organizationAnswersDir, survey.slug)
  if (!fs.existsSync(surveyAnswersDir)) {
    fs.mkdirSync(surveyAnswersDir)
  }
  const answerFilePath = path.join(surveyAnswersDir, `${token}.json`)
  fs.writeFileSync(answerFilePath, JSON.stringify(answer, null, 2))

  res.writeHead(200, {
    "Content-Type": "application/json",
  })
  res.end(JSON.stringify({ answer, token }, null, 2))
}

function validateBody(survey, body, ipAddress) {
  if (body === null || body === undefined) {
    return [body, "Missing body"]
  }
  if (typeof body !== "object") {
    return [body, `Expected an object, got ${typeof body}`]
  }

  body = { ...body }
  const remainingKeys = new Set(Object.keys(body))
  const errors = {}

  {
    const key = "answer"
    remainingKeys.delete(key)
    const [value, error] = validateSurveyAnswer(survey, body[key], ipAddress)
    body[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "token"
    remainingKeys.delete(key)
    const [value, error] = survey.tokens
      ? validateRegExp(shortTokenRegExp, "Expected a short token")(body[key])
      : validateRegExp(uuidTokenRegExp, "Expected a UUID token")(body[key])
    body[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [body, Object.keys(errors).length === 0 ? null : errors]
}
