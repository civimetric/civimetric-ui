import { getOrganization, getSurvey } from "../../../../model"

function generateSurveyQf(survey) {
  let content = ""

  content += `title\r\n  |${survey.title}|\r\n\r\n`
  content += "running title\r\n  ||\r\n\r\n"
  content += "spend_counter\r\n"
  content += "language french\r\n"
  content += "buttons close\r\n"
  if (survey.randomize) {
    content += "randomise\r\n"
  }
  content += "unistatic\r\n"
  content += "hover_time 2000\r\n"

  for (let [gridName, grid] of Object.entries(survey.grids)) {
    content += "\r\n"
    content += `grid |${gridName}| =\r\n`
    for (let question of grid) {
      content += `question |${question.title}|\r\n`

      content += "  options\r\n"
      for (let option of question.options) {
        content += `    |${option}|\r\n`
      }

      content += "  prices\r\n"
      for (let price of question.prices) {
        content += `    ${price}\r\n`
      }

      content += "\r\n"
    }
  }

  // Create QF-specific block for introduction.
  {
    content += "demographic block |introduction|\r\n\r\n"
    content += `  text |${survey.introduction.trim()}|\r\n`
    content += "\r\n"
  }

  for (let block of survey.blocks) {
    const qfBlockType = block.type === "information" ? "demographic" : block.type

    if (block.type === "demographic") {
      content += `${qfBlockType} block |${block.slug}|\r\n\r\n`

      const header = (block.header_html || "").trim()
      if (header) {
        content += `  text |${header}|\r\n`
      }

      const gridSummary = block.grid_summary
      if (gridSummary) {
        content += "\r\n&\r\n\r\n"
        content += `  grid_summary |${gridSummary.grid}|\r\n`
        content += "    columns\r\n"
        for (let column of gridSummary.columns) {
          content += `      |${column}|\r\n`
        }
        if (gridSummary.differences) {
          content += "    differences\r\n"
        }
        content += "\r\n&\r\n\r\n||\r\n\r\n"
      }

      for (let question of block.questions || []) {
        if (question.type === "select") {
          content += `  question |${question.title || question.slug}|\r\n`
          content += `    long |${(question.label || "").trim()}|\r\n`
          content += "    options\r\n"
          for (let option of question.options) {
            content += `      |${option}|\r\n`
          }
          content += "    layout horizontal layers 1\r\n"
          const bounds = question.bounds
          content += `    bounds ${bounds[0]}, ${bounds[1]}\r\n`
        } else if (question.type === "textarea") {
          content += `  open question |${question.title || question.slug}|\r\n`
          content += `    long |${(question.label || "").trim()}|\r\n`
          content += `    rows ${question.rows}\r\n`
          content += "    columns 80\r\n"
        } else {
          content += `    UNKNOWN QUESTION TYPE: ${question.type}\r\n`
        }

        content += "\r\n"
      }

      const footer = (block.footer_html || "").trim()
      if (footer) {
        content += `  text |${footer}|\r\n`
      }
    } else if (block.type === "dynamic grid") {
      content += `${qfBlockType} block |${block.slug}|\r\n\r\n`

      const header = (block.header_html || "").trim()
      if (header) {
        content += `  text |${header}|\r\n`
      }

      content += `  grid |${block.grid}|\r\n`
      content += `  bonus ${block.bonus}\r\n`
      content += `  tolerance ${block.tolerance}\r\n`
      if (block.initially === null) {
        const grid = survey.grids[block.grid]
        const initially = grid.map((question) => "1").join(" ") // eslint-disable-line no-unused-vars
        content += `  initially ${initially} show\r\n`
      } else {
        content += `  initially |${block.initially}|\r\n`
      }
      content += "  gridlines\r\n"
    } else if (block.type === "importance") {
      content += `${qfBlockType} block |${block.slug}|\r\n\r\n`

      content += "  question ||\r\n"
      content += `    grid |${block.grid}|\r\n`

      const long = [block.header_html, block.footer_html]
        .filter((html) => html)
        .join("\r\n")
        .trim()
      content += `    long |${long}|\r\n`

      const grid = survey.grids[block.grid]
      if (grid) {
        content += "    options\r\n"
        let options = grid.map((question) => question.title)
        for (let option of options) {
          content += `      |${option}|\r\n`
        }
      }

      content += "    layout\r\n"
      content += "      vertical layers 1\r\n"

      const bounds = block.bounds
      content += "    bounds\r\n"
      content += `      ${bounds[0]}, ${bounds[1]}\r\n`
    } else if (block.type === "information") {
      content += `${qfBlockType} block |${block.slug}|\r\n\r\n`

      const text = [block.header_html, block.body_html, block.footer_html]
        .filter((html) => html)
        .join("\r\n")
        .trim()
      content += `  text |${text}|\r\n`
    } else if (block.type === "static grid") {
      content += `${qfBlockType} block\r\n\r\n`

      const header = (block.header_html || "").trim()
      if (header) {
        content += `  text |${header}|\r\n`
      }

      content += `  grid |${block.grid}|\r\n`
      content += `  task |${block.current}| current\r\n`
      content += `  task |${block.acceptable}| unacceptable\r\n`
      content += "  unitext ||\r\n"
    } else {
      content += `UNKNOWN BLOCK TYPE: ${block.type}\r\n`
    }

    content += "\r\n"
  }

  content += "label |Last stage|\r\n"
  content += "\r\n"
  content += "label |End|\r\n"

  return content
}

export function get(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthorized: User is not authenticated.",
          },
        },
        null,
        2,
      ),
    )
  }
  if (!user.isAdmin) {
    res.writeHead(401, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Forbidden: User is not an administrator.",
          },
        },
        null,
        2,
      ),
    )
  }

  const { organization: organizationSlug, survey: surveySlug } = req.params
  const organization = getOrganization(organizationSlug)

  if (!organization) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Organization ${organizationSlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  const survey = getSurvey(organization, surveySlug)

  if (!survey) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Survey ${surveySlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "text/plain; charset=utf-8",
    "Content-Disposition": `attachment; filename="${survey.slug}.qf"`,
  })
  res.end(generateSurveyQf(survey))
}
