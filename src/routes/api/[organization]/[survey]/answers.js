import { getOrganization, getSurvey, getSurveyAnswers } from "../../../../model"

export function get(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthorized: User is not authenticated.",
          },
        },
        null,
        2,
      ),
    )
  }
  if (!user.isAdmin) {
    res.writeHead(401, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Forbidden: User is not an administrator.",
          },
        },
        null,
        2,
      ),
    )
  }

  const { organization: organizationSlug, survey: surveySlug } = req.params
  const organization = getOrganization(organizationSlug)

  if (!organization) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Organization ${organizationSlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  const survey = getSurvey(organization, surveySlug)

  if (!survey) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Survey ${surveySlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  const surveyAnswers = getSurveyAnswers(organization, survey)

  if (!surveyAnswers) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Answers of survey ${surveySlug} don't exist yet.`,
          },
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json",
  })
  res.end(JSON.stringify(surveyAnswers, null, 2))
}
