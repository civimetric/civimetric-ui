import Papa from "papaparse"

import { getOrganization, getSurvey, getSurveyAnswers } from "../../../../model"

function addFieldToAccessTree(fieldsAccessTree, field, accessorIndex = 0) {
  if (accessorIndex >= field.access.length) {
    if (fieldsAccessTree.leafs === undefined) {
      fieldsAccessTree.leafs = []
    }
    fieldsAccessTree.leafs.push(field)
    return
  }
  const accessor = field.access[accessorIndex]
  if (fieldsAccessTree.branches === undefined) {
    fieldsAccessTree.branches = []
  } else {
    for (const branch of fieldsAccessTree.branches) {
      if (equalAccessor(branch.accessor, accessor)) {
        return addFieldToAccessTree(branch.tree, field, accessorIndex + 1)
      }
    }
  }
  const branch = {
    accessor,
    tree: {},
  }
  fieldsAccessTree.branches.push(branch)
  return addFieldToAccessTree(branch.tree, field, accessorIndex + 1)
}

function buildFieldsAccessTree(fields) {
  const fieldsAccessTree = {}
  for (const field of fields) {
    addFieldToAccessTree(fieldsAccessTree, field)
  }
  return fieldsAccessTree
}

export function equalAccessor(accessor1, accessor2) {
  if (accessor1 === undefined || accessor2 === undefined) {
    return accessor1 === accessor2
  }
  if (accessor1.operation !== accessor2.operation) {
    return false
  }
  switch (accessor1.operation) {
    case "array":
      return true
    case "array_item":
      return accessor1.index === accessor2.index
    case "object_item":
      return accessor1.key === accessor2.key
    default:
      throw new Error(`Unexpected accessor operation: "${accessor1.operation}"`)
  }
}

function generateSurveyCsv(survey, surveyAnswers) {
  const fields = [
    {
      access: [
        { operation: "array" },
        { operation: "array_item", index: 1 },
        { operation: "object_item", key: "ipAddressDigest" },
      ],
      name: "IP address digest",
    },
    {
      access: [{ operation: "array" }, { operation: "array_item", index: 0 }],
      name: "token",
    },
    {
      access: [
        { operation: "array" },
        { operation: "array_item", index: 1 },
        { operation: "object_item", key: "startDateTime" },
      ],
      name: "started",
    },
    {
      access: [
        { operation: "array" },
        { operation: "array_item", index: 1 },
        { operation: "object_item", key: "stopDateTime" },
      ],
      name: "submitted",
    },
    {
      access: [
        { operation: "array" },
        { operation: "array_item", index: 1 },
        { operation: "object_item", key: "geo" },
        { operation: "object_item", key: "region" },
      ],
      name: "region",
    },
    {
      access: [
        { operation: "array" },
        { operation: "array_item", index: 1 },
        { operation: "object_item", key: "geo" },
        { operation: "object_item", key: "country" },
      ],
      name: "country",
    },
    {
      access: [
        { operation: "array" },
        { operation: "array_item", index: 1 },
        { operation: "object_item", key: "geo" },
        { operation: "object_item", key: "city" },
      ],
      name: "city",
    },
  ]
  for (const block of survey.blocks) {
    const blockAccess = [
      { operation: "array" },
      { operation: "array_item", index: 1 },
      { operation: "object_item", key: block.slug },
    ]
    if (block.type === "demographic") {
      for (const question of block.questions) {
        const questionAccess = [...blockAccess, { operation: "object_item", key: question.slug }]
        if (question.type === "select") {
          fields.push({
            access: question.bounds[1] === 1 ? questionAccess : [...questionAccess, { operation: "array" }],
            name: `${block.title} / ${question.title}`,
          })
        } else if (question.type === "textarea") {
          fields.push({
            access: questionAccess,
            name: `${block.title} / ${question.title}`,
          })
        } else {
          console.log(`Unknown question type: ${question.type}`)
        }
      }
    } else if (block.type === "dynamic grid") {
      const grid = survey.grids[block.grid]
      if (!grid) {
        console.log(`Unknown grid: ${block.grid}`)
        continue
      }
      for (const question of grid) {
        fields.push({
          access: [...blockAccess, { operation: "object_item", key: question.slug }],
          name: `${block.title} / ${question.title}`,
        })
      }
    } else if (block.type === "importance") {
      const grid = survey.grids[block.grid]
      if (!grid) {
        console.log(`Unknown grid: ${block.grid}`)
        continue
      }
      fields.push({
        access: [...blockAccess, { operation: "object_item", key: "importance" }, { operation: "array" }],
        name: `${block.title} / importance`,
      })
    } else if (block.type === "information") {
      // Ignore this block: it has no question.
    } else if (block.type === "static grid") {
      const grid = survey.grids[block.grid]
      if (!grid) {
        console.log(`Unknown grid: ${block.grid}`)
        continue
      }
      for (const question of grid) {
        fields.push({
          access: [...blockAccess, { operation: "object_item", key: `current.${question.slug}` }],
          name: `${block.title} / ${question.title} / current`,
        })
        fields.push({
          access: [
            ...blockAccess,
            { operation: "object_item", key: `acceptable.${question.slug}` },
            { operation: "array" },
          ],
          name: `${block.title} / ${question.title} / acceptable`,
        })
      }
    } else {
      console.log(`Unknown block type: ${block.type}`)
    }
  }

  const fieldsAccessTree = buildFieldsAccessTree(fields)
  const entries = [...iterDataEntriesFromFieldsAccessTree(fieldsAccessTree, Object.entries(surveyAnswers))]

  return Papa.unparse(entries, {
    columns: fields.map((field) => field.name),
    delimiter: ",",
  })
}

export function get(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthorized: User is not authenticated.",
          },
        },
        null,
        2,
      ),
    )
  }
  if (!user.isAdmin) {
    res.writeHead(401, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Forbidden: User is not an administrator.",
          },
        },
        null,
        2,
      ),
    )
  }

  const { organization: organizationSlug, survey: surveySlug } = req.params
  const organization = getOrganization(organizationSlug)

  if (!organization) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Organization ${organizationSlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  const survey = getSurvey(organization, surveySlug)

  if (!survey) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Survey ${surveySlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  const surveyAnswers = getSurveyAnswers(organization, survey)

  if (!surveyAnswers) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Answers of survey ${surveySlug} don't exist yet.`,
          },
        },
        null,
        2,
      ),
    )
  }
  res.writeHead(200, {
    "Content-Type": "text/csv; charset=utf-8",
    "Content-Disposition": `attachment; filename="${survey.slug}.csv"`,
  })
  res.end(generateSurveyCsv(survey, surveyAnswers))
}

function* iterDataEntriesBranches(branches, branchIndex, dataExtract, entry) {
  if (branchIndex >= branches.length) {
    yield entry
    return
  }
  const { accessor, tree } = branches[branchIndex]
  switch (accessor.operation) {
    case "array": {
      if (
        dataExtract === undefined ||
        dataExtract === null ||
        !Array.isArray(dataExtract) ||
        dataExtract.length === 0
      ) {
        break
      }
      for (const value of dataExtract) {
        for (const branchEntry of iterDataEntriesBranches(branches, branchIndex + 1, dataExtract, entry)) {
          yield* iterDataEntriesTree(tree, value, branchEntry)
        }
      }
      return
    }
    case "array_item": {
      if (dataExtract === undefined || dataExtract === null || !Array.isArray(dataExtract)) {
        break
      }
      const value = dataExtract[accessor.index]
      for (const branchEntry of iterDataEntriesBranches(branches, branchIndex + 1, dataExtract, entry)) {
        yield* iterDataEntriesTree(tree, value, branchEntry)
      }
      return
    }
    case "object_item": {
      if (dataExtract === undefined || dataExtract === null || typeof dataExtract !== "object") {
        break
      }
      const value = dataExtract[accessor.key]
      for (const branchEntry of iterDataEntriesBranches(branches, branchIndex + 1, dataExtract, entry)) {
        yield* iterDataEntriesTree(tree, value, branchEntry)
      }
      return
    }
    default:
      throw new Error(`Unexpected accessor operation: "${accessor.operation}"`)
  }

  for (const branchEntry of iterDataEntriesBranches(branches, branchIndex + 1, dataExtract, entry)) {
    yield* iterDataEntriesTree(tree, undefined, branchEntry)
  }
}

export function* iterDataEntriesFromFieldsAccessTree(fieldsAccessTree, data) {
  for (const entry of iterDataEntriesTree(fieldsAccessTree, data, {})) {
    if (Object.keys(entry).length > 0) {
      yield entry
    }
  }
}

function* iterDataEntriesTree(fieldsAccessTree, dataExtract, entry) {
  if (dataExtract === undefined) {
    yield entry
    return
  }
  if (fieldsAccessTree.leafs !== undefined) {
    entry = { ...entry }
    for (const field of fieldsAccessTree.leafs) {
      entry[field.name] = dataExtract
    }
  }
  if (fieldsAccessTree.branches === undefined) {
    yield entry
  } else {
    yield* iterDataEntriesBranches(fieldsAccessTree.branches, 0, dataExtract, entry)
  }
}
