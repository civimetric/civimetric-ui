import path from "path"

import { getOrganization, getSurveys } from "../../../model"
import serverConfig from "../../../server-config"

const { organizationsDir } = serverConfig

export function get(req, res) {
  const { organization: organizationSlug } = req.params
  const organizationDir = path.join(organizationsDir, organizationSlug)
  const organization = getOrganization(organizationSlug)
  if (!organization) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Organization ${organizationSlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json",
  })
  const surveys = getSurveys(organizationDir)
  res.end(
    JSON.stringify(
      {
        ...organization,
        surveys,
      },
      null,
      2,
    ),
  )
}
