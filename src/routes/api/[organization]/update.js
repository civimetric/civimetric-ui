import { execFileSync } from "child_process"
import fs from "fs"
import path from "path"

import { generateOrganization, generateSurveys } from "../../../generators"
import { getOrganization } from "../../../model"

export function post(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthorized: User is not authenticated.",
          },
        },
        null,
        2,
      ),
    )
  }
  if (!user.isAdmin) {
    res.writeHead(401, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Forbidden: User is not an administrator.",
          },
        },
        null,
        2,
      ),
    )
  }

  const { organization: organizationSlug } = req.params
  const organization = getOrganization(organizationSlug)

  if (!organization) {
    res.writeHead(404, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `Organization ${organizationSlug} doesn't exist.`,
          },
        },
        null,
        2,
      ),
    )
  }

  if (!organization.repository) {
    res.writeHead(400, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message: `Organization ${organizationSlug} has no Git repository.`,
          },
        },
        null,
        2,
      ),
    )
  }

  const env = {}
  const sshKeyFilePath = path.join(organization.dir, "id_rsa")
  if (fs.existsSync(sshKeyFilePath)) {
    env["GIT_SSH_COMMAND"] = `ssh -i ${sshKeyFilePath}`
  }
  try {
    execFileSync("git", ["pull"], {
      cwd: organization.dir,
      env,
      timeout: 30000, // 30 seconds
    })
  } catch (e) {
    res.writeHead(400, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message: `Command "git pull" failed, with error:\n${e}`,
          },
        },
        null,
        2,
      ),
    )
  }

  const [generatedOrganization, error] = generateOrganization(organizationSlug)
  if (error !== null) {
    res.writeHead(400, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            details: error,
            message: "Generation of organization failed.",
          },
          organization: generatedOrganization,
        },
        null,
        2,
      ),
    )
  }

  const [generatedSurveys, surveysErrors] = generateSurveys(organization)
  if (surveysErrors !== null) {
    res.writeHead(400, {
      "Content-Type": "application/json",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            details: surveysErrors,
            message: "Generation of surveys failed.",
          },
          organization: generatedOrganization,
          surveys: generatedSurveys,
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json",
  })
  res.end(
    JSON.stringify(
      {
        organization: generatedOrganization,
        surveys: generatedSurveys,
      },
      null,
      2,
    ),
  )
}
