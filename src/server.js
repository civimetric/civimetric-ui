import "./global.scss"

import bodyParser from "body-parser"
import compression from "compression"
import { pbkdf2Sync } from "crypto"
import session from "express-session"
import passport from "passport"
import { Strategy as LocalStrategy } from "passport-local"
import path from "path"
import polka from "polka"
import makeSessionFileStore from "session-file-store"
import sirv from "sirv"
import { Store } from "svelte/store"

import * as sapper from "@sapper/server"
import { getUser } from "./model"
import serverConfig from "./server-config"

// const { PORT, NODE_ENV } = process.env
// const dev = NODE_ENV === "development"
const { PORT } = process.env
const SessionFileStore = makeSessionFileStore(session)
const { organizationsDir, sessionsDir, sessionSecret, title } = serverConfig

passport.deserializeUser(function (email, done) {
  const user = getUser(email)
  done(null, user)
})

passport.serializeUser(function (user, done) {
  done(null, user.email)
})

passport.use(
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
    },
    function (email, password, done) {
      const user = getUser(email)
      if (user === null) {
        return done(null, false, { email: `No user with email "${email}".` })
      }
      let passwordDigest = pbkdf2Sync(password, user.salt, 4096, 16, "sha512").toString("base64").replace(/=/g, "")
      if (passwordDigest != user.passwordDigest) {
        return done(null, false, {
          password: `Invalid password for user "${email}".`,
        })
      }
      return done(null, user)
    },
  ),
)

polka() // You can also use Express
  // Sirv 0.2.0 doesn't work in dev mode.
  // .use(compression({ threshold: 0 }), sirv("static", { dev }))
  // .use("/organizations", sirv(path.join(organizationsDir, ".."), { dev }))
  .use(compression({ threshold: 0 }), sirv("static"))
  .use(
    session({
      // cookie: {
      //   maxAge: 31536000
      // },
      resave: false,
      saveUninitialized: false,
      secret: sessionSecret,
      store: new SessionFileStore({
        path: sessionsDir,
      }),
      unset: "destroy",
    }),
  )
  .use("/organizations", sirv(path.join(organizationsDir, "..")))
  .use(bodyParser.json())
  .use(passport.initialize())
  .use(passport.session())
  .use(
    sapper.middleware({
      session: (req, res) => ({
        title,
        user: req.user,
      }),
    }),
  )
  .listen(PORT, (error) => {
    if (error) {
      console.log(`Error when calling listen on port ${PORT}:`, error)
    }
  })
