export function gotoRetrievalError(goto) {
  return (location, url, response, result, error) => {
    return goto(
      response.status,
      `Error at ${location}, while accessing "${url}": ${JSON.stringify(error, null, 2)}\n\n${JSON.stringify(
        result,
        null,
        2,
      )}`,
    )
  }
}

export function logRetrievalError(location, url, response, result, error) {
  console.error(
    response.status,
    `Error at ${location}, while accessing ${url}: ${JSON.stringify(error, null, 2)}\n\n${JSON.stringify(
      result,
      null,
      2,
    )}`,
  )
  return {
    ...result,
    error: {
      error,
      location,
      status: response.status,
      url,
    },
  }
}

export function quiet404RetrievalError(errorHandler) {
  return (location, url, response, result, error) => {
    return response.status === 404
      ? quietRetrievalError(location, url, response, result, error)
      : errorHandler(location, url, response, result, error)
  }
}

export function quietRetrievalError(location, url, response, result, error) {
  return {
    ...result,
    error: {
      error,
      location,
      status: response.status,
      url,
    },
  }
}
