import { validateServerConfig } from "./validators/server-config"

const serverConfig = {
  dataDir: "../data/",
  sessionSecret: "civimetric secret", // Change it!
  title: "Civimetric Surveys",
}

const [validServerConfig, error] = validateServerConfig(serverConfig)
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(validServerConfig, null, 2)}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default validServerConfig
