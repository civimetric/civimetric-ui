// Server functions to generate organizations & surveys

import fs from "fs"
import yaml from "js-yaml"
import marked from "marked"
import path from "path"
import resolveUrl from "url-resolve"

import serverConfig from "./server-config"
import {
  shortTokenRegExp,
  validateArray,
  validateBoolean,
  validateChoice,
  validateInteger,
  validateJson,
  validateMaybeBoolean,
  validateMayBeTrimmedString,
  validateNonEmptyTrimmedString,
  validateRegExp,
} from "./validators/core"
import { validateMayBeTextFile } from "./validators/files"

const { accountsDir, organizationsDir } = serverConfig

const blockTypes = ["demographic", "dynamic grid", "importance", "information", "static grid"]
const questionTypes = ["select", "textarea"]

const renderer = new marked.Renderer()
const originIndependentUrl = /^$|^[a-z][a-z0-9+.-]*:|^[?#]/i
renderer.image = function (href, title, text) {
  if (this.options.baseUrl && !originIndependentUrl.test(href)) {
    href = resolveUrl(this.options.baseUrl, href)
  }
  text = text ? ` alt="${text}"` : ""
  title = title ? ` title="${title}"` : ""
  const slash = this.options.xhtml ? "/" : ""
  return `<img${text} class="img-fluid" src="${href}"${title}${slash}>`
}
const markedOptions = {
  renderer,
}

function generateBlock(organization, survey, blocks, block) {
  if (block === null || block === undefined) {
    return [block, "Missing block"]
  }
  if (typeof block !== "object") {
    return [block, `Expected an object, got ${typeof block}`]
  }

  block = { ...block }
  const errors = {}
  const remainingKeys = new Set(Object.keys(block))

  {
    const key = "slug"
    remainingKeys.delete(key)
    const [value, error] = validateRegExp(/^[a-z][-a-z0-9]*[a-z0-9]$/, "Expected a slug")(block[key])
    block[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "title"
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(block[key])
    block[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "type"
    remainingKeys.delete(key)
    let [value, error] = validateNonEmptyTrimmedString(block[key])
    block[key] = value
    if (error === null && !blockTypes.includes(value)) {
      const expectedValues = blockTypes.map((blockType) => `"${blockType}"`).join(", ")
      error = `Expected one of ${expectedValues}`
    }
    if (error !== null) {
      errors[key] = error
    }
  }

  if (!errors.type) {
    if (block.type === "demographic") {
      for (let key of ["footer_html", "header_html"]) {
        remainingKeys.delete(key)
        const [value, error] = validateMayBeTrimmedString(block[key])
        block[key] = value
        if (error !== null) {
          errors[key] = error
        }
      }

      {
        const key = "grid_summary"
        remainingKeys.delete(key)
        if (![null, undefined].includes(block[key])) {
          const [value, error] = generateGridSummary(survey, block[key])
          block[key] = value
          if (error !== null) {
            errors[key] = error
          }
        }
      }

      {
        const key = "questions"
        remainingKeys.delete(key)
        if (![null, undefined].includes(block[key])) {
          const [value, error] = validateArray(generateQuestion(organization, survey))(block[key])
          block[key] = value
          if (error !== null) {
            errors[key] = error
          }
        }
      }
    } else if (block.type === "dynamic grid") {
      {
        const key = "bonus"
        remainingKeys.delete(key)
        let [value, error] = validateInteger(block[key])
        block[key] = value
        if (error !== null) {
          errors[key] = error
        }
      }
      block.spending_limit = block.bonus

      {
        const key = "enable_low_spending"
        remainingKeys.delete(key)
        const [value, error] = validateBoolean(block[key])
        block[key] = value
        if (error !== null) {
          errors[key] = error
        }
      }

      {
        const key = "grid"
        remainingKeys.delete(key)
        let [value, error] = validateNonEmptyTrimmedString(block[key])
        block[key] = value
        if (error === null && survey.grids[value] === undefined) {
          error = "Unknown grid name"
        }
        if (error !== null) {
          errors[key] = error
        }
      }

      for (let key of ["footer_html", "header_html"]) {
        remainingKeys.delete(key)
        const [value, error] = validateMayBeTrimmedString(block[key])
        block[key] = value
        if (error !== null) {
          errors[key] = error
        }
      }

      {
        const key = "initially"
        remainingKeys.delete(key)
        if (![null, undefined].includes(block[key])) {
          let [value, error] = validateRegExp(/^[a-z][-a-z0-9]*[a-z0-9]$/, "Expected a slug")(block[key])
          block[key] = value
          if (error === null) {
            const baseBlock = blocks.find((baseBlock) => baseBlock.slug === block.initially)
            if (baseBlock === undefined) {
              error = "Base block doesn't exist"
            } else if (baseBlock.spending_limit === undefined) {
              error = "Base block must be defined before this block"
            } else {
              block.spending_limit += baseBlock.spending_limit
            }
          }
          if (error !== null) {
            errors[key] = error
          }
        }
      }

      {
        const key = "spending_limit"
        remainingKeys.delete(key)
        let [value, error] = validateInteger(block[key])
        block[key] = value
        if (error !== null) {
          errors[key] = error
        }
      }

      {
        const key = "tolerance"
        remainingKeys.delete(key)
        let [value, error] = validateInteger(block[key])
        block[key] = value
        if (error === null && value < 0) {
          error = "Expected a positive number or zero"
        }
        if (error !== null) {
          errors[key] = error
        }
      }
    } else if (block.type === "importance") {
      {
        const key = "bounds"
        remainingKeys.delete(key)
        let [value, error] = generateBounds(block[key])
        block[key] = value
        if (error !== null) {
          errors[key] = error
        }
      }

      {
        const key = "grid"
        remainingKeys.delete(key)
        let [value, error] = validateNonEmptyTrimmedString(block[key])
        block[key] = value
        if (error === null && survey.grids[value] === undefined) {
          error = "Unknown grid name"
        }
        if (error !== null) {
          errors[key] = error
        }
      }

      for (let key of ["footer_html", "header_html"]) {
        remainingKeys.delete(key)
        const [value, error] = validateMayBeTrimmedString(block[key])
        block[key] = value
        if (error !== null) {
          errors[key] = error
        }
      }
    } else if (block.type === "information") {
      for (let key of ["body_html", "footer_html", "header_html"]) {
        remainingKeys.delete(key)
        const [value, error] = validateMayBeTrimmedString(block[key])
        block[key] = value
        if (error !== null) {
          errors[key] = error
        }
      }
    } else if (block.type === "static grid") {
      for (let key of ["acceptable", "current"]) {
        remainingKeys.delete(key)
        const [value, error] = validateNonEmptyTrimmedString(block[key])
        block[key] = value
        if (error !== null) {
          errors[key] = error
        }
      }

      {
        const key = "grid"
        remainingKeys.delete(key)
        let [value, error] = validateNonEmptyTrimmedString(block[key])
        block[key] = value
        if (error === null && survey.grids[value] === undefined) {
          error = "Unknown grid name"
        }
        if (error !== null) {
          errors[key] = error
        }
      }

      for (let key of ["footer_html", "header_html"]) {
        remainingKeys.delete(key)
        const [value, error] = validateMayBeTrimmedString(block[key])
        block[key] = value
        if (error !== null) {
          errors[key] = error
        }
      }
    } else {
      errors.type = "Missing code to generate this type of block"
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }

  return [block, Object.keys(errors).length === 0 ? null : errors]
}

export function generateBlocks(organization, survey, blocks) {
  if (blocks === null || blocks === undefined) {
    return [blocks, "Missing value"]
  }
  if (!Array.isArray(blocks)) {
    return [blocks, `Expected an array, got "${typeof blocks}"`]
  }
  const errors = {}
  blocks = [...blocks]
  for (let [index, value] of blocks.entries()) {
    const [validatedValue, error] = generateBlock(organization, survey, blocks, value)
    blocks[index] = validatedValue
    if (error !== null) {
      errors[index] = error
    }
  }
  return [blocks, Object.keys(errors).length === 0 ? null : errors]
}

function generateBounds(bounds) {
  if (bounds === null || bounds === undefined) {
    return [bounds, "Missing value"]
  }
  if (!Array.isArray(bounds)) {
    return [bounds, `Expected an array, got "${typeof bounds}"`]
  }
  if (bounds.length !== 2) {
    return [bounds, `Array must have 2 items, got ${bounds.length}`]
  }

  bounds = [...bounds]
  const errors = {}

  {
    const [value, error] = validateInteger(bounds[0])
    bounds[0] = value
    if (error === null && value < 0) {
      error = "Expected a positive number or zero"
    }
    if (error !== null) {
      errors["0"] = error
    }
  }

  {
    const [value, error] = validateInteger(bounds[1])
    bounds[1] = value
    if (error === null && !errors["0"] && value < bounds[0]) {
      error = `Expected a number greater than or equal to ${bounds[0]}`
    }
    if (error !== null) {
      errors["1"] = error
    }
  }

  return [bounds, Object.keys(errors).length === 0 ? null : errors]
}

function generateGrid(organization, survey, gridName, grid) {
  if (grid === null || grid === undefined) {
    return [grid, "Missing value"]
  }
  if (!Array.isArray(grid)) {
    return [grid, `Expected an array, got "${typeof grid}"`]
  }

  const errors = {}
  grid = [...grid]
  const gridDir = path.join(survey.dir, "grids", gridName)
  const remainingMarkdownFilenames =
    !fs.existsSync(gridDir) || !fs.lstatSync(gridDir).isDirectory()
      ? new Set()
      : new Set(fs.readdirSync(gridDir).filter((filename) => !filename.startsWith(".") && filename.endsWith(".md")))

  for (let [index, value] of grid.entries()) {
    const [validatedValue, error] = ((question) => {
      if (question === null || question === undefined) {
        return [question, "Missing value"]
      }
      if (typeof question !== "object") {
        return [question, `Expected an object, got "${typeof question}"`]
      }

      const errors = {}
      question = { ...question }
      const remainingKeys = new Set(Object.keys(question))

      {
        const key = "image"
        remainingKeys.delete(key)
        let [value, error] = validateNonEmptyTrimmedString(question[key])
        if (error === null) {
          const filePath = path.join(gridDir, value)
          if (!fs.existsSync(filePath) || !fs.lstatSync(filePath).isFile()) {
            error = `Invalid file path "${filePath}"`
          }
          value = resolveUrl(`/organizations/${organization.slug}/${survey.slug}/grids/${gridName}/`, value)
        }
        question[key] = value
        if (error !== null) {
          errors[key] = error
        }
      }

      {
        const key = "options"
        remainingKeys.delete(key)
        const [value, error] = validateArray(validateNonEmptyTrimmedString)(question[key])
        question[key] = value
        if (error === null && value.length === 0) {
          error = "Array must contain at least one item"
        }
        if (error !== null) {
          errors[key] = error
        }
      }

      {
        const key = "prices"
        remainingKeys.delete(key)
        let [value, error] = validateArray(validateInteger)(question[key])
        question[key] = value
        if (error === null && question.options.length !== question.prices.length) {
          error = `Lengths of options & prices arrays don't match (${question.options.length} != ${question.prices.length})`
        }
        if (error !== null) {
          errors[key] = error
        }
      }

      {
        const key = "slug"
        remainingKeys.delete(key)
        const [value, error] = validateRegExp(/^[a-z][-a-z0-9]*[a-z0-9]$/, "Expected a slug")(question[key])
        question[key] = value
        if (error !== null) {
          errors[key] = error
        }
      }

      if (!errors.slug) {
        const key = "moreInfo"
        const markdownFilename = `${question.slug}.md`
        if (remainingMarkdownFilenames.delete(markdownFilename)) {
          const markdownFilePath = path.join(gridDir, markdownFilename)
          const markdownText = fs.readFileSync(markdownFilePath, "utf-8")
          question[key] = markdownToHtml(markdownText, {
            baseUrl: `/organizations/${organization.slug}/${survey.slug}/grids/${gridName}/`,
          })
        }
      }

      {
        const key = "title"
        remainingKeys.delete(key)
        const [value, error] = validateNonEmptyTrimmedString(question[key])
        question[key] = value
        if (error !== null) {
          errors[key] = error
        }
      }

      for (let key of remainingKeys) {
        errors[key] = "Unexpected entry"
      }

      return [question, Object.keys(errors).length === 0 ? null : errors]
    })(value)
    grid[index] = validatedValue
    if (error !== null) {
      errors[index] = error
    }
  }

  for (let filename of remainingMarkdownFilenames) {
    const basename = path.basename(filename, ".md")
    errors[basename] = `Grid has no question named "${basename}", but a "${filename}" file exists`
  }

  return [
    grid,
    Object.keys(errors).length === 0 ? (grid.length === 0 ? "Grid must contain at least one item" : null) : errors,
  ]
}

export function generateGrids(organization, survey, grids) {
  if (grids === null || grids === undefined) {
    return [grids, "Missing value"]
  }
  if (typeof grids !== "object") {
    return [grids, `Expected an object, got "${typeof grids}"`]
  }
  const errors = {}
  grids = { ...grids }
  for (let [key, value] of Object.entries(grids)) {
    const [validatedKey, keyError] = validateNonEmptyTrimmedString(key)
    const [validatedValue, valueError] = generateGrid(organization, survey, validatedKey, value)
    grids[validatedKey] = validatedValue
    if (keyError !== null) {
      errors[validatedKey] = keyError
    } else if (valueError !== null) {
      errors[validatedKey] = valueError
    }
  }
  return [grids, Object.keys(errors).length === 0 ? null : errors]
}

export function generateGridSummary(survey, gridSummary) {
  if (gridSummary === null || gridSummary === undefined) {
    return [gridSummary, "Missing grid summary"]
  }
  if (typeof gridSummary !== "object") {
    return [gridSummary, `Expected an object, got ${typeof gridSummary}`]
  }

  gridSummary = { ...gridSummary }
  const errors = {}
  const remainingKeys = new Set(Object.keys(gridSummary))

  {
    const key = "columns"
    remainingKeys.delete(key)
    const [value, error] = validateArray(validateNonEmptyTrimmedString)(gridSummary[key])
    gridSummary[key] = value
    if (error === null && value.length === 0) {
      error = "Array must contain at least one item"
    }
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "differences"
    remainingKeys.delete(key)
    const [value, error] = validateBoolean(gridSummary[key])
    gridSummary[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "grid"
    remainingKeys.delete(key)
    let [value, error] = validateNonEmptyTrimmedString(gridSummary[key])
    gridSummary[key] = value
    if (error === null && survey.grids[value] === undefined) {
      error = "Unknown grid name"
    }
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }

  return [gridSummary, Object.keys(errors).length === 0 ? null : errors]
}

export function generateOrganization(organizationSlug) {
  const organizationDir = path.join(organizationsDir, organizationSlug)
  if (!fs.existsSync(organizationDir)) {
    return [null, `Directory "${organizationDir}" doesn't exist`]
  }
  if (!fs.lstatSync(organizationDir).isDirectory()) {
    return [null, `Node "${organizationDir}" isn't a directory`]
  }
  const organizationFilePath = path.join(organizationDir, "index.yaml")
  if (!fs.existsSync(organizationFilePath)) {
    return [null, `File "${organizationFilePath}" doesn't exist`]
  }
  if (!fs.lstatSync(organizationFilePath).isFile()) {
    return [null, `Node "${organizationFilePath}" isn't a file`]
  }
  const organizationText = fs.readFileSync(organizationFilePath)
  let organization = null
  try {
    organization = yaml.safeLoad(organizationText)
  } catch (e) {
    return [organization, `${e}`]
  }

  if (organization === null || organization === undefined) {
    return [organization, "Missing organization"]
  }
  if (typeof organization !== "object") {
    return [organization, `Expected an object, got ${typeof organization}`]
  }

  organization = { ...organization }
  organization.dir = organizationDir
  organization.slug = organizationSlug

  const errors = {}
  const remainingKeys = new Set(Object.keys(organization))
  const remainingMarkdownFilenames = new Set(
    fs.readdirSync(organizationDir).filter((filename) => !filename.startsWith(".") && filename.endsWith(".md")),
  )

  {
    const key = "banner"
    const markdownFilename = `${key}.md`
    if (remainingMarkdownFilenames.delete(markdownFilename)) {
      const markdownFilePath = path.join(organizationDir, markdownFilename)
      const markdownText = fs.readFileSync(markdownFilePath, "utf-8")
      organization[key] = markdownToHtml(markdownText, {
        baseUrl: `/organizations/${organization.slug}/`,
      })
    }
  }

  for (let key of ["dir", "name", "repository", "slug"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(organization[key])
    organization[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let rsaKeyFilename of ["id_rsa", "id_rsa.pub"]) {
    const rsaKeyFilePath = path.join(organizationDir, rsaKeyFilename)
    const [rsaKey, error] = validateMayBeTextFile(validateNonEmptyTrimmedString)(rsaKeyFilePath)
    if (
      error === null &&
      rsaKey !== null &&
      !rsaKeyFilename.endsWith(".pub") &&
      (fs.statSync(rsaKeyFilePath).mode & 0x3f) !== 0
    ) {
      error = "Private key file must not be accessible by others"
    }
    if (error !== null) {
      errors[rsaKeyFilename] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  for (let filename of remainingMarkdownFilenames) {
    const basename = path.basename(filename, ".md")
    errors[basename] = `Organization has no component named "${basename}", but a "${filename}" file exists`
  }

  if (Object.keys(errors).length !== 0) {
    return [organization, errors]
  }

  const generatedOrganizationFilePath = path.join(organizationDir, "organization.json")
  fs.writeFileSync(generatedOrganizationFilePath, JSON.stringify(organization, null, 2))

  return [organization, null]
}

function generateQuestion(organization, survey) {
  return function (question) {
    if (question === null || question === undefined) {
      return [question, "Missing question"]
    }
    if (typeof question !== "object") {
      return [question, `Expected an object, got ${typeof question}`]
    }

    question = { ...question }
    const errors = {}
    const remainingKeys = new Set(Object.keys(question))

    {
      const key = "label"
      remainingKeys.delete(key)
      let [value, error] = validateMayBeTrimmedString(question[key])
      question[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    {
      const key = "required"
      remainingKeys.delete(key)
      const [value, error] = validateBoolean(question[key])
      question[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    {
      const key = "slug"
      remainingKeys.delete(key)
      const [value, error] = validateRegExp(/^[a-z][-a-z0-9]*[a-z0-9]$/, "Expected a slug")(question[key])
      question[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    {
      const key = "title"
      remainingKeys.delete(key)
      const [value, error] = validateMayBeTrimmedString(question[key])
      question[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    {
      const key = "type"
      remainingKeys.delete(key)
      let [value, error] = validateNonEmptyTrimmedString(question[key])
      question[key] = value
      if (error === null && !questionTypes.includes(value)) {
        const expectedValues = questionTypes.map((questionType) => `"${questionType}"`).join(", ")
        error = `Expected one of ${expectedValues}`
      }
      if (error !== null) {
        errors[key] = error
      }
    }

    if (!errors.type) {
      if (question.type === "select") {
        {
          const key = "bounds"
          remainingKeys.delete(key)
          let [value, error] = generateBounds(question[key])
          question[key] = value
          if (error !== null) {
            errors[key] = error
          }
        }

        {
          const key = "images"
          remainingKeys.delete(key)
          const input = question[key]
          const [value, error] = [null, undefined].includes(input)
            ? [null, null]
            : validateArray((input) => {
                let [value, error] = validateNonEmptyTrimmedString(input)
                if (error === null) {
                  const filePath = path.join(survey.dir, value)
                  if (!fs.existsSync(filePath) || !fs.lstatSync(filePath).isFile()) {
                    error = `Invalid file path "${filePath}"`
                  }
                  value = resolveUrl(`/organizations/${organization.slug}/${survey.slug}/`, value)
                }
                return [value, error]
              })(input)
          question[key] = value
          // if (error === null && value !== null && value.length === 0) {
          //   error = "Array must contain at least one item"
          // }
          if (error !== null) {
            errors[key] = error
          }
        }

        {
          const key = "menu"
          remainingKeys.delete(key)
          let [value, error] = validateMaybeBoolean(question[key])
          question[key] = value
          if (error !== null) {
            errors[key] = error
          }
        }

        {
          const key = "options"
          remainingKeys.delete(key)
          const [value, error] = validateArray(validateNonEmptyTrimmedString)(question[key])
          question[key] = value
          if (error === null && value.length === 0) {
            error = "Array must contain at least one item"
          }
          if (error !== null) {
            errors[key] = error
          }
        }
      } else if (question.type === "textarea") {
        {
          const key = "rows"
          remainingKeys.delete(key)
          const [value, error] = validateInteger(question[key])
          question[key] = value
          if (error === null && value < 1) {
            error = "Expected a positive number"
          }
          if (error !== null) {
            errors[key] = error
          }
        }
      } else {
        errors.type = "Missing code to generate this type of question"
      }
    }

    for (let key of remainingKeys) {
      errors[key] = "Unexpected entry"
    }

    return [question, Object.keys(errors).length === 0 ? null : errors]
  }
}

export function generateSurvey(organization, surveySlug) {
  const surveyDir = path.join(organization.dir, surveySlug)
  if (!fs.existsSync(surveyDir)) {
    return [null, `Directory "${surveyDir}" doesn't exist`]
  }
  if (!fs.lstatSync(surveyDir).isDirectory()) {
    return [null, `Node "${surveyDir}" isn't a directory`]
  }
  const surveyFilePath = path.join(surveyDir, "index.yaml")
  if (!fs.existsSync(surveyFilePath)) {
    return [null, `File "${surveyFilePath}" doesn't exist`]
  }
  if (!fs.lstatSync(surveyFilePath).isFile()) {
    return [null, `Node "${surveyFilePath}" isn't a file`]
  }
  const surveyText = fs.readFileSync(surveyFilePath)
  let survey = null
  try {
    survey = yaml.safeLoad(surveyText)
  } catch (e) {
    return [survey, `${e}`]
  }

  if (survey === null || survey === undefined) {
    return [survey, "Missing survey"]
  }
  if (typeof survey !== "object") {
    return [survey, `Expected an object, got ${typeof survey}`]
  }

  survey = { ...survey }
  survey.dir = surveyDir
  survey.slug = surveySlug

  const errors = {}
  const remainingKeys = new Set(Object.keys(survey))
  const remainingMarkdownFilenames = new Set(
    fs.readdirSync(surveyDir).filter((filename) => !filename.startsWith(".") && filename.endsWith(".md")),
  )

  {
    const key = "banner"
    const markdownFilename = `${key}.md`
    if (remainingMarkdownFilenames.delete(markdownFilename)) {
      const markdownFilePath = path.join(surveyDir, markdownFilename)
      const markdownText = fs.readFileSync(markdownFilePath, "utf-8")
      survey[key] = markdownToHtml(markdownText, {
        baseUrl: `/organizations/${organization.slug}/${survey.slug}/`,
      })
    }
  }

  for (let key of ["conclusion", "introduction"]) {
    const markdownFilename = `${key}.md`
    if (remainingMarkdownFilenames.delete(markdownFilename)) {
      const markdownFilePath = path.join(surveyDir, markdownFilename)
      const markdownText = fs.readFileSync(markdownFilePath, "utf-8")
      survey[key] = markdownToHtml(markdownText, {
        baseUrl: `/organizations/${organization.slug}/${survey.slug}/`,
      })
    } else {
      errors[key] = `Missing "${markdownFilename}" Markdown file in survey`
    }
  }

  for (let key of ["dir", "slug", "title"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(survey[key])
    survey[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "grids"
    remainingKeys.delete(key)
    const [value, error] = generateGrids(organization, survey, survey[key])
    survey[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  if (!errors.grids) {
    const key = "blocks"
    remainingKeys.delete(key)
    const [value, error] = generateBlocks(organization, survey, survey[key])
    survey[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "randomize"
    remainingKeys.delete(key)
    const value = survey[key]
    const error = [false, null, true, undefined].includes(value) ? null : `Expected a boolean, got ${typeof value}`
    if (error === null) {
      survey[key] = !!value
    } else {
      errors[key] = error
    }
  }

  {
    const key = "storage"
    remainingKeys.delete(key)
    const [value, error] = validateChoice(["local", "session"])(survey[key])
    survey[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const tokensFilename = "tokens.json"
    const tokensFilePath = path.join(accountsDir, organization.slug, surveySlug, tokensFilename)
    const [tokens, error] = validateMayBeTextFile(
      validateJson(validateArray(validateRegExp(shortTokenRegExp, "Expected a short token"))),
    )(tokensFilePath)
    if (error === null) {
      survey.tokens = tokens !== null
    } else {
      survey.tokens = tokens
      errors["tokens"] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  for (let filename of remainingMarkdownFilenames) {
    const basename = path.basename(filename, ".md")
    errors[basename] = `Survey has no component named "${basename}", but a "${filename}" file exists`
  }

  if (Object.keys(errors).length !== 0) {
    return [survey, errors]
  }

  const generatedSurveyFilePath = path.join(surveyDir, "survey.json")
  fs.writeFileSync(generatedSurveyFilePath, JSON.stringify(survey, null, 2))

  return [survey, null]
}

export function generateSurveys(organization) {
  const surveysSlugs = fs
    .readdirSync(organization.dir)
    .filter((filename) => !filename.startsWith("."))
    .filter((filename) => {
      const surveyDir = path.join(organization.dir, filename)
      if (!fs.lstatSync(surveyDir).isDirectory()) {
        return false
      }
      const surveyFilePath = path.join(surveyDir, "index.yaml")
      return fs.existsSync(surveyFilePath) && fs.lstatSync(surveyFilePath).isFile()
    })
  const errors = {}
  const surveys = []
  for (let [surveyIndex, surveySlug] of surveysSlugs.entries()) {
    const [survey, surveyError] = generateSurvey(organization, surveySlug)
    surveys.push(survey)
    if (surveyError !== null) {
      errors[surveyIndex] = surveyError
    }
  }
  return [surveys, Object.keys(errors).length === 0 ? null : errors]
}

function markdownToHtml(text, options) {
  options = {
    ...markedOptions,
    ...(options || {}),
  }
  text = text.replace(/src="(.*?)"/g, (match, url) => {
    if (options.baseUrl && !originIndependentUrl.test(url)) {
      url = resolveUrl(options.baseUrl, url)
    }
    return `src="${url}"`
  })
  return marked(text, options)
}
