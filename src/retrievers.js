import { logRetrievalError } from "./errors"
import { shuffle } from "./helpers"

export async function retrieveBlock(
  organizationSlug,
  surveySlug,
  blockSlug,
  { errorHandler = logRetrievalError, fetch, location = "retrieveBlock" } = {},
) {
  const url = `/api/${organizationSlug}/${surveySlug}/${blockSlug}`
  const response = await fetch(url)
  const result = await response.json()
  if (result.error) {
    return errorHandler(location, url, response, result, result.error)
  }
  return result
}

export async function retrieveOrganization(
  organizationSlug,
  { errorHandler = logRetrievalError, fetch, location = "retrieveOrganization" } = {},
) {
  const url = `/api/${organizationSlug}`
  const response = await fetch(url)
  const result = await response.json()
  if (result.error) {
    return errorHandler(location, url, response, result, result.error)
  }
  return result
}

export async function retrieveOrganizations({
  errorHandler = logRetrievalError,
  fetch,
  location = "retrieveOrganizations",
} = {}) {
  const url = "/api"
  const response = await fetch(url)
  const result = await response.json()
  if (result.error) {
    return errorHandler(location, url, response, result, result.error)
  }
  const { organizations } = result
  return organizations
}

export async function retrieveSurvey(
  organizationSlug,
  surveySlug,
  { errorHandler = logRetrievalError, fetch, location = "retrieveSurvey" } = {},
) {
  const url = `/api/${organizationSlug}/${surveySlug}`
  const response = await fetch(url)
  const result = await response.json()
  if (result.error) {
    return errorHandler(location, url, response, result, result.error)
  }
  let survey = result
  if (survey.randomize) {
    survey = { ...survey, grids: { ...survey.grids } }
    for (let [gridName, grid] of Object.entries(survey.grids)) {
      survey.grids[gridName] = shuffle(grid)
    }
  }
  return survey
}

export async function retrieveSurveyAnswers(
  organizationSlug,
  surveySlug,
  { errorHandler = logRetrievalError, fetch, location = "retrieveSurveyAnswers" } = {},
) {
  const url = `/api/${organizationSlug}/${surveySlug}/answers`
  const response = await fetch(url)
  const result = await response.json()
  if (result.error) {
    return errorHandler(location, url, response, result, result.error)
  }
  return result
}
