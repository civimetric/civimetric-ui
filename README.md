# Civimetric-UI

_Web user interface & API for Civimetric surveys_

## Installation

```bash
npm install
```

### Launch server

```bash
npm run build
npm run dev
```
