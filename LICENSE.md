# Civimetric-UI

_Web user interface & API for Civimetric surveys_

By: Emmanuel Raviart <emmanuel@raviart.com>

Copyright (C) 2018 Emmanuel Raviart

https://framagit.org/civimetric/civimetric-ui

> Civimetric-UI is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> Civimetric-UI is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
